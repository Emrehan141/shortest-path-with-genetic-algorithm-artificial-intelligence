# # # # # # # #
# # # # # # # #
# # # # # # # #   Ekteki map.png ile aynı klasördeyken çalıştırmalıdır.
# # # # # # # #
# # # # # # # #
# # # # # # # #
# # # # # # # #

# # # # # # # #

import numpy as np
import numbers
import cv2

#################
#  Bu projedeki veri yapısını şu şekilde oluşturduk.
#  Popülasyon, kromozomlardan oluşuyor. Kromozomlar genlerden oluşuyor.
#  Her gen 3 elemandan oluşuyor.  1. ilçe, 2. ilçe ve bu yolun maliyeti [I1, I2, M]
#  Her kromozom A noktasından B noktasına KESİNLİKLE ulaşan DEĞİŞKEN sayıda genden oluşuyor.
#  İki farklı kromozom örneği (A noktasından E noktasına ulaşmaya çalışıyor)
#  [A, B, 3] [B, D, 5] [D, E, 2] => Toplam maliyet = 10
#  [A, E, 11] => Toplam maliyet = 11
#
#  Popülasyon aynı noktadan başlayıp aynı noktaya varan farklı kromozomlardan oluşuyor. (Örnekteki gibi)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Crossover:
#  [A, B, 3] [B, D, 5] [D, E, 2]
#  [A, E, 11]
#  [A, C, 6] [C, B, 3] [B, E, 4]
# şeklinde 3 kromozomlu bir popülasyonumuz olsun.
#
# 1. kromozomun 1. geninden ( A,B,3 ) için diğer kromozomlardan B ile başlayan gen arıyoruz.
# 3. kromozomun son geni (B,E,4) B ile başladığı için bunları birleştiriyoruz ve yeni kromozom oluşuyor
# [A, B, 3] [B, E, 4] => Toplam maliyet = 7
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#   Mutasyon
#
#   Rastgele seçilen bir kromozomun rastgele bir genine kadar alıp, kalan kısmın yeniden oluşturulmasıyla oluşturuluyor.
#   Rastgele gelen kromozom : [A, B, 3] [B, D, 5] [D, C, 2] [C, E, 2]
#   Rastgele gelen gen 2. gen olsun. 2. gene kadar tüm genler aynı kalıyor.
#   GEN =>  [A, B, 3] [B, D, 5] |||||||||||||||||
#   Burdan sonra problem D noktasından E noktasına gitme problemine dönüşüyor.
#   Kromozom oluşturma fonksiyonu ile bu sorunu çözen kromozom üretiliyor
#   Oluşan kromozom [D, B, 3] [B, E, 2] bu iki kromozomu birleştiriyoruz
#   [A, B, 3] [B, D, 5] [D, B, 3] [B, E, 2] böylece mutasyonla yeni kromozom oluşuyor.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#   Doğruluk Oranı
#
#   Elimizde tüm şehirlerin birbirine olan en kısa uzaklığı olmadığı için her adımda yolun doğruluğunu
#   gözle kontrol edebiliyoruz. Biz raporda google maps 'i kullandık.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#   Kabuller
#
#   Bir şehirden diğer şehre sadece sınır varsa geçiyor
#   Sadece Konyadaki ilçelerden geçiş var, yan şehirlere geçmiyor.
################

# İlçelerin ID'si , Adı ve resimdeki koordinatları
def initCitiesInfos():
    citiesInfos = []

    citiesInfos.append([0, "Ahırlı", (150,440)])
    citiesInfos.append([1, "Akören", (190,400)])
    citiesInfos.append([2, "Akşehir", (60,200)])
    citiesInfos.append([3, "Altınekin", (292,216)])
    citiesInfos.append([4, "Beyşehir", (88,341)])
    citiesInfos.append([5, "Bozkır", (193,454)])
    citiesInfos.append([6, "Cihanbeyli", (284,129)])
    citiesInfos.append([7, "Çeltik", (124,60)])
    citiesInfos.append([8, "Çumra", (267,384)])
    citiesInfos.append([9, "Derbent", (151,281)])
    citiesInfos.append([10, "Derebucak", (65,401)])
    citiesInfos.append([11, "Doğanhisar", (89,250)])
    citiesInfos.append([12, "Emirgazi", (446,295)])
    citiesInfos.append([13, "Ereğli", (485,356)])
    citiesInfos.append([14, "Güneysınır", (259,428)])
    citiesInfos.append([15, "Hadim", (214,492)])
    citiesInfos.append([16, "Halkapınar", (513,412)])
    citiesInfos.append([17, "Hüyük", (72,289)])
    citiesInfos.append([18, "Ilgın", (130,228)])
    citiesInfos.append([19, "Kadınhanı", (180,201)])
    citiesInfos.append([20, "Karapınar", (377,338)])
    citiesInfos.append([21, "Karatay", (296,304)])
    citiesInfos.append([22, "Kulu", (327,58)])
    citiesInfos.append([23, "Meram", (182,337)])
    citiesInfos.append([24, "Sarayönü", (219,196)])
    citiesInfos.append([25, "Selçuklu", (232,271)])
    citiesInfos.append([26, "Seydişehir", (123,392)])
    citiesInfos.append([27, "Taşkent", (231,518)])
    citiesInfos.append([28, "Tuzlukçu", (87,172)])
    citiesInfos.append([29, "Yalıhüyük", (149,424)])
    citiesInfos.append([30, "Yunak", (146,117)])
    return citiesInfos

#İlçelerin birbirine olan uzaklıkları. Sadece birbirine komşu olanların arasındaki
# KM farkı yazıldı. Komşu değilse -1
def initGraph(citiesInfos):
    graph = np.full((len(citiesInfos),len(citiesInfos)),-1)

    graph[0][1] = 52
    graph[0][2] = -1
    graph[0][3] = -1
    graph[0][4] = -1
    graph[0][5] = 16
    graph[0][6] = -1
    graph[0][7] =-1
    graph[0][8] =-1
    graph[0][9] =-1
    graph[0][10] =-1
    graph[0][11] =-1
    graph[0][12] =-1
    graph[0][13] =-1
    graph[0][14] =-1
    graph[0][15] =-1
    graph[0][16] =-1
    graph[0][17] =-1
    graph[0][18] =-1
    graph[0][19] =-1
    graph[0][20] =-1
    graph[0][21] =-1
    graph[0][22] =-1
    graph[0][23] =-1
    graph[0][24] =-1
    graph[0][25] =-1
    graph[0][26] = 40
    graph[0][27] =-1
    graph[0][28] =-1
    graph[0][29] = 8
    graph[0][30] =-1

    graph[1][2] = -1
    graph[1][3] = -1
    graph[1][4] = -1
    graph[1][5] = 47
    graph[1][6] = -1
    graph[1][7] = -1
    graph[1][8] = 40
    graph[1][9] = -1
    graph[1][10] = -1
    graph[1][11] = -1
    graph[1][12] = -1
    graph[1][13] = -1
    graph[1][14] = -1
    graph[1][15] = -1
    graph[1][16] = -1
    graph[1][17] = -1
    graph[1][18] = -1
    graph[1][19] = -1
    graph[1][20] = -1
    graph[1][21] = -1
    graph[1][22] = -1
    graph[1][23] = 58
    graph[1][24] = -1
    graph[1][25] = -1
    graph[1][26] = 60
    graph[1][27] = -1
    graph[1][28] = -1
    graph[1][29] = -1
    graph[1][30] = -1

    graph[2][3] = -1
    graph[2][4] = -1
    graph[2][5] = -1
    graph[2][6] = -1
    graph[2][7] = -1
    graph[2][8] = -1
    graph[2][9] = -1
    graph[2][10] = -1
    graph[2][11] = 47
    graph[2][12] = -1
    graph[2][13] = -1
    graph[2][14] = -1
    graph[2][15] = -1
    graph[2][16] = -1
    graph[2][17] = -1
    graph[2][18] = 47
    graph[2][19] = -1
    graph[2][20] = -1
    graph[2][21] = -1
    graph[2][22] = -1
    graph[2][23] = -1
    graph[2][24] = -1
    graph[2][25] = -1
    graph[2][26] = -1
    graph[2][27] = -1
    graph[2][28] = 28
    graph[2][29] = -1
    graph[2][30] = -1

    graph[3][4] = -1
    graph[3][5] = -1
    graph[3][6] = 50
    graph[3][7] = -1
    graph[3][8] = -1
    graph[3][9] = -1
    graph[3][10] = -1
    graph[3][11] = -1
    graph[3][12] = -1
    graph[3][13] = -1
    graph[3][14] = -1
    graph[3][15] = -1
    graph[3][16] = -1
    graph[3][17] = -1
    graph[3][18] = -1
    graph[3][19] = -1
    graph[3][20] = -1
    graph[3][21] = 100
    graph[3][22] = -1
    graph[3][23] = -1
    graph[3][24] = 42
    graph[3][25] = 63
    graph[3][26] = -1
    graph[3][27] = -1
    graph[3][28] = -1
    graph[3][29] = -1
    graph[3][30] = -1

    graph[4][5] = -1
    graph[4][6] = -1
    graph[4][7] = -1
    graph[4][8] = -1
    graph[4][9] = 56
    graph[4][10] = 45
    graph[4][11] = -1
    graph[4][12] = -1
    graph[4][13] = -1
    graph[4][14] = -1
    graph[4][15] = -1
    graph[4][16] = -1
    graph[4][17] = 35
    graph[4][18] = 86
    graph[4][19] = -1
    graph[4][20] = -1
    graph[4][21] = -1
    graph[4][22] = -1
    graph[4][23] = 74
    graph[4][24] = -1
    graph[4][25] = -1
    graph[4][26] = 33
    graph[4][27] = -1
    graph[4][28] = -1
    graph[4][29] = -1
    graph[4][30] = -1

    graph[5][6] = -1
    graph[5][7] = -1
    graph[5][8] = 90
    graph[5][9] = -1
    graph[5][10] = -1
    graph[5][11] = -1
    graph[5][12] = -1
    graph[5][13] = -1
    graph[5][14] = 55
    graph[5][15] = 74
    graph[5][16] = -1
    graph[5][17] = -1
    graph[5][18] = -1
    graph[5][19] = -1
    graph[5][20] = -1
    graph[5][21] = -1
    graph[5][22] = -1
    graph[5][23] = -1
    graph[5][24] = -1
    graph[5][25] = -1
    graph[5][26] = -1
    graph[5][27] = -1
    graph[5][28] = -1
    graph[5][29] = -1
    graph[5][30] = -1

    graph[6][7] = -1
    graph[6][8] = -1
    graph[6][9] = -1
    graph[6][10] = -1
    graph[6][11] = -1
    graph[6][12] = -1
    graph[6][13] = -1
    graph[6][14] = -1
    graph[6][15] = -1
    graph[6][16] = -1
    graph[6][17] = -1
    graph[6][18] = -1
    graph[6][19] = -1
    graph[6][20] = -1
    graph[6][21] = -1
    graph[6][22] = 50
    graph[6][23] = -1
    graph[6][24] = 75
    graph[6][25] = -1
    graph[6][26] = -1
    graph[6][27] = -1
    graph[6][28] = -1
    graph[6][29] = -1
    graph[6][30] = 142

    graph[7][8] = -1
    graph[7][9] = -1
    graph[7][10] = -1
    graph[7][11] = -1
    graph[7][12] = -1
    graph[7][13] = -1
    graph[7][14] = -1
    graph[7][15] = -1
    graph[7][16] = -1
    graph[7][17] = -1
    graph[7][18] = -1
    graph[7][19] = -1
    graph[7][20] = -1
    graph[7][21] = -1
    graph[7][22] = -1
    graph[7][23] = -1
    graph[7][24] = -1
    graph[7][25] = -1
    graph[7][26] = -1
    graph[7][27] = -1
    graph[7][28] = -1
    graph[7][29] = -1
    graph[7][30] = 30

    graph[8][9] = -1
    graph[8][10] = -1
    graph[8][11] = -1
    graph[8][12] = -1
    graph[8][13] = -1
    graph[8][14] = 37
    graph[8][15] = -1
    graph[8][16] = -1
    graph[8][17] = -1
    graph[8][18] = -1
    graph[8][19] = -1
    graph[8][20] = 80
    graph[8][21] = 58
    graph[8][22] = -1
    graph[8][23] = 72
    graph[8][24] = -1
    graph[8][25] = -1
    graph[8][26] = -1
    graph[8][27] = -1
    graph[8][28] = -1
    graph[8][29] = -1
    graph[8][30] = -1

    graph[9][10] = -1
    graph[9][11] = -1
    graph[9][12] = -1
    graph[9][13] = -1
    graph[9][14] = -1
    graph[9][15] = -1
    graph[9][16] = -1
    graph[9][17] = -1
    graph[9][18] = 52
    graph[9][19] = 44
    graph[9][20] = -1
    graph[9][21] = -1
    graph[9][22] = -1
    graph[9][23] = 64
    graph[9][24] = -1
    graph[9][25] = 61
    graph[9][26] = -1
    graph[9][27] = -1
    graph[9][28] = -1
    graph[9][29] = -1
    graph[9][30] = -1

    graph[10][11] = -1
    graph[10][12] = -1
    graph[10][13] = -1
    graph[10][14] = -1
    graph[10][15] = -1
    graph[10][16] = -1
    graph[10][17] = -1
    graph[10][18] = -1
    graph[10][19] = -1
    graph[10][20] = -1
    graph[10][21] = -1
    graph[10][22] = -1
    graph[10][23] = -1
    graph[10][24] = -1
    graph[10][25] = -1
    graph[10][26] = 50
    graph[10][27] = -1
    graph[10][28] = -1
    graph[10][29] = -1
    graph[10][30] = -1


    graph[11][12] = -1
    graph[11][13] = -1
    graph[11][14] = -1
    graph[11][15] = -1
    graph[11][16] = -1
    graph[11][17] = 33
    graph[11][18] = 35
    graph[11][19] = -1
    graph[11][20] = -1
    graph[11][21] = -1
    graph[11][22] = -1
    graph[11][23] = -1
    graph[11][24] = -1
    graph[11][25] = -1
    graph[11][26] = -1
    graph[11][27] = -1
    graph[11][28] = -1
    graph[11][29] = -1
    graph[11][30] = -1

    graph[12][13] = 60
    graph[12][14] = -1
    graph[12][15] = -1
    graph[12][16] = -1
    graph[12][17] = -1
    graph[12][18] = -1
    graph[12][19] = -1
    graph[12][20] = 38
    graph[12][21] = -1
    graph[12][22] = -1
    graph[12][23] = -1
    graph[12][24] = -1
    graph[12][25] = -1
    graph[12][26] = -1
    graph[12][27] = -1
    graph[12][28] = -1
    graph[12][29] = -1
    graph[12][30] = -1

    graph[13][14] = -1
    graph[13][15] = -1
    graph[13][16] = 16
    graph[13][17] = -1
    graph[13][18] = -1
    graph[13][19] = -1
    graph[13][20] = 52
    graph[13][21] = -1
    graph[13][22] = -1
    graph[13][23] = -1
    graph[13][24] = -1
    graph[13][25] = -1
    graph[13][26] = -1
    graph[13][27] = -1
    graph[13][28] = -1
    graph[13][29] = -1
    graph[13][30] = -1

    graph[14][15] = -1
    graph[14][16] = -1
    graph[14][17] = -1
    graph[14][18] = -1
    graph[14][19] = -1
    graph[14][20] = -1
    graph[14][21] = -1
    graph[14][22] = -1
    graph[14][23] = -1
    graph[14][24] = -1
    graph[14][25] = -1
    graph[14][26] = -1
    graph[14][27] = -1
    graph[14][28] = -1
    graph[14][29] = -1
    graph[14][30] = -1

    graph[15][16] = -1
    graph[15][17] = -1
    graph[15][18] = -1
    graph[15][19] = -1
    graph[15][20] = -1
    graph[15][21] = -1
    graph[15][22] = -1
    graph[15][23] = -1
    graph[15][24] = -1
    graph[15][25] = -1
    graph[15][26] = -1
    graph[15][27] = 12
    graph[15][28] = -1
    graph[15][29] = -1
    graph[15][30] = -1

    graph[16][17] = -1
    graph[16][18] = -1
    graph[16][19] = -1
    graph[16][20] = -1
    graph[16][21] = -1
    graph[16][22] = -1
    graph[16][23] = -1
    graph[16][24] = -1
    graph[16][25] = -1
    graph[16][26] = -1
    graph[16][27] = -1
    graph[16][28] = -1
    graph[16][29] = -1
    graph[16][30] = -1

    graph[17][18] = 64
    graph[17][19] = -1
    graph[17][20] = -1
    graph[17][21] = -1
    graph[17][22] = -1
    graph[17][23] = -1
    graph[17][24] = -1
    graph[17][25] = -1
    graph[17][26] = -1
    graph[17][27] = -1
    graph[17][28] = -1
    graph[17][29] = -1
    graph[17][30] = -1

    graph[18][19] = 28
    graph[18][20] = -1
    graph[18][21] = -1
    graph[18][22] = -1
    graph[18][23] = -1
    graph[18][24] = -1
    graph[18][25] = -1
    graph[18][26] = -1
    graph[18][27] = -1
    graph[18][28] = 42
    graph[18][29] = -1
    graph[18][30] = 87

    graph[19][20] = -1
    graph[19][21] = -1
    graph[19][22] = -1
    graph[19][23] = -1
    graph[19][24] = 25
    graph[19][25] = 49
    graph[19][26] = -1
    graph[19][27] = -1
    graph[19][28] = -1
    graph[19][29] = -1
    graph[19][30] = 96

    graph[20][21] = 57
    graph[20][22] = -1
    graph[20][23] = -1
    graph[20][24] = -1
    graph[20][25] = -1
    graph[20][26] = -1
    graph[20][27] = -1
    graph[20][28] = -1
    graph[20][29] = -1
    graph[20][30] = -1


    graph[21][22] = -1
    graph[21][23] = 82
    graph[21][24] = -1
    graph[21][25] = 64
    graph[21][26] = -1
    graph[21][27] = -1
    graph[21][28] = -1
    graph[21][29] = -1
    graph[21][30] = -1

    graph[22][23] = -1
    graph[22][24] = -1
    graph[22][25] = -1
    graph[22][26] = -1
    graph[22][27] = -1
    graph[22][28] = -1
    graph[22][29] = -1
    graph[22][30] = -1

    graph[23][24] = -1
    graph[23][25] = 44
    graph[23][26] = 68
    graph[23][27] = -1
    graph[23][28] = -1
    graph[23][29] = -1
    graph[23][30] = -1

    graph[24][25] = 38
    graph[24][26] = -1
    graph[24][27] = -1
    graph[24][28] = -1
    graph[24][29] = -1
    graph[24][30] = 120

    graph[25][26] = -1
    graph[25][27] = -1
    graph[25][28] = -1
    graph[25][29] = -1
    graph[25][30] = -1

    graph[26][27] = -1
    graph[26][28] = -1
    graph[26][29] = -1
    graph[26][30] = -1

    graph[27][28] = -1
    graph[27][29] = -1
    graph[27][30] = -1

    graph[28][29] = -1
    graph[28][30] = 200

    graph[29][30] = -1

    for i in range(0, len(citiesInfos)):
        for j in range(0, i):
            graph[i][j]=graph[j][i]

    return graph

# Verilen noktadan, RASTGELE bir komşusuna giden gen üretir.
def initGen(currentPlace, graph):
    gen = np.zeros(shape=(3),dtype=np.int)
    gen[0] = int(currentPlace) # Nerede

    gen[1], gen[2] = possibleTravels(currentPlace, graph)
    while gen[1] == -1:
        gen[1],gen[2] = possibleTravels(currentPlace,graph) # Nereye gidiyor # Maliyet

    return gen

# Verilen noktadan, hedef noktaya giden bir kromozom oluşturur
def initKrom(currentPlace, targetPlace, maxLen, graph):
    graphLen = len(graph[0])
    gen = []
    notVisited = np.ones(graphLen, dtype=bool)

    cp = currentPlace
    notVisited[cp]=False

    gen.append(initGen(cp,graph))
    cp = gen[len(gen) - 1][1]

    while(len(gen)<=maxLen and gen[len(gen)-1][1]!=targetPlace and notVisited[cp]):
        notVisited[cp]=False
        gen.append(initGen(cp,graph))
        cp = gen[len(gen)-1][1]

    if gen[len(gen)-1][1]==targetPlace:
        return gen
    else:
        return -1

# Verilen sayıda kromozom oluşturur
def initPop(popSize, currentPlace, targetPlace, maxLen, graph):
    pop = []
    count = 0
    iter = 0
    while(count<popSize and iter<100000):
        tmpKrom = initKrom(currentPlace,targetPlace,maxLen,graph)
        if(tmpKrom!=-1):
            if not isExist(tmpKrom,pop):
                pop.append(tmpKrom)
                count+=1
        iter+=1

    if iter>=100000:
        balancePop(pop,popSize)
    return pop

# Verilen bir kromozomun verilen popülasyon içinde olup olmadığına bakar
def isExist(krom, pop):
    #print("Pop", pop)
    #printPop(pop)
    #print("Krom",krom)
    if len(pop)!=0 and not isinstance(krom, numbers.Integral):
        for oneKrom in pop:
            if len(krom)==len(oneKrom):
                for i in range(0, len(krom)):
                    for j in range(0,3):
                        if krom[i][j]!=oneKrom[i][j]:
                            #print("krom[i][j]!=oneKrom[i][j] ", krom[i], " ", oneKrom[i])
                            return False
        return True
    if len(pop)==0:
        return False
    return True

# Verilen noktanın rastgele bir komşusunu ve oraya gitmenin maliyetini döndürür
def possibleTravels(currentPlace,graph):
    i=0
    index=-1
    notFound = -1
    graphLen = len(graph[0])

    while(i<graphLen and notFound==-1):
        index = np.random.randint(0, graphLen)
        while(index==currentPlace):
            index = np.random.randint(0, graphLen)
        notFound = graph[currentPlace][index]
        i+=1
    if notFound==-1:
        return -1,-1

    return index, notFound

# Verilen popülasyondaki tüm kromozomların maliyetini ve maliyetine göre sıralamasını döndürür
def createDistanceMatris(pop, graph):
    cost = np.zeros(shape=(len(pop)),dtype=int)
    c = 0
    for krom in pop:
        for gen in krom:
            cost[c]+=gen[2]
        c+=1
    return cost, np.argsort(cost)

# Verilen oranda mutasyon uygular. Detaylı açıklama 26. satırda
def mutasyon(popSize, mutasyonRatio, pop, graph, maxLen):
    #print("LEN => ",len(pop))
    for i in range(0,int(len(pop)*mutasyonRatio/100)):
        randomKromozom = np.random.randint(0, len(pop))
        while(len(pop[randomKromozom])<=2):
            randomKromozom = np.random.randint(0, popSize)

        randomGen = np.random.randint(1, len(pop[randomKromozom])-1)

        newGen = initKrom(pop[randomKromozom][randomGen][1],pop[randomKromozom][len(pop[randomKromozom])-1][1], maxLen, graph)
        while(newGen==-1):
            newGen = initKrom(pop[randomKromozom][randomGen][1], pop[randomKromozom][len(pop[randomKromozom]) - 1][1],
                              maxLen, graph)

        #print("**********")
        #print(pop[randomKromozom][:randomGen + 1])
        #print(newGen[:])

        mutasyonGen = []

        for gen in pop[randomKromozom][:randomGen + 1]:
            mutasyonGen.append(gen)
        for gen in newGen:
            mutasyonGen.append(gen)

        #print("Kullanılan dna ",randomKromozom," ",randomGen)
        pop[randomKromozom]= np.copy(mutasyonGen)

# Crossover yardımcı bir fonksiyon. Verilen bir genden sonra gelebilecek bir genin adresini buluyor.
def findSameGen(target, searchRange, pop, notThis):
    result = []
    for i in searchRange:
        for j in range(0,len(pop[i])):
            if target == pop[i][j][0] and i != notThis:
                result.append([i,j])
    return -1 if len(result)==0 else result

# Detaylı açıklama 16. satırda
def crossover(pop):
    popSize = len(pop)
    newPop = []

    p=0
    genI = 0
    while len(newPop)<popSize and p<len(pop):
        if len(pop[p])==genI+1:
            if len(newPop)!=0 :
                if not isExist(pop[p],newPop):
                    newPop.append(pop[p])
                    #print("Eklendi => ",p)
                    p+=1
                    genI=0
                    if p>=len(pop):
                        break
                        #print("YENI P ve GENI => ",p," ",genI)
                    continue
            else:
                newPop.append(pop[p])
                #print("Eklendi => ", p)
                p += 1
                genI = 0
                if p >= len(pop):
                    break
                    #print("YENI P ve GENI => ", p, " ", genI)
                continue

        if genI >= len(pop[p]):
            #print("genI >= len(pop[p])  ",p," ",genI)
            p+=1
            if p >= len(pop):
                break
            genI=0

            #print("P => ",p)
            #print("Same Gen")
            #print("P G",p," ",genI)
        fsg = findSameGen(pop[p][genI][1], range(0, len(pop)), pop, p)
        if fsg == -1:
            genI+=1
            continue
            #print(fsg)

        for gen in fsg:
            newGen = []

            for g in pop[p][:genI + 1]:
                newGen.append(g)
            for g in pop[gen[0]][gen[1]:]:
                newGen.append(g)

            if len(newPop)!=0:
                if not isExist(newGen,newPop):
                    newPop.append(newGen)
            else:
                newPop.append(newGen)

        genI += 1
        #print("POP -1", newPop)
    #print("YENİ POP")
    #printPop(newPop)
        #print("POP SIZE!!!!!", len(newPop))
        #print("POP 0 => ", newPop)

    #printPop(newPop)
    #input("devam?")
    #print("newpop len",len(newPop))

    balancePop(pop,popSize)
    return newPop[:popSize]

# Verilen distance matrisine göre popülasyonu maliyete göre sıralar
def sortPop(pop, distance):
    new_pop = []
    for d in distance:
        new_pop.append(pop[d])
    return new_pop

# Popülasyonu yazdırır
def printPop(pop):
    for i in range(0,len(pop)):
        print(pop[i])

# Resmi okuyup citiesInfos 'da belirtilan koordinatlara nokta ve id'sini koyar.
def initImage(imageName, citiesInfos):
    map = cv2.imread(imageName, 1)

    for city in citiesInfos:
        cv2.circle(map, (city[2][0], city[2][1]), 3, [0, 0, 255], 3)
        cv2.putText(map, str(city[0]), (city[2][0], city[2][1]), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,
                    [255, 0, 0])

    return map

# Verilen popülasyondaki en iyi yolu çizer
def drawPaths(map, pop, citiesInfos, iter):
    cost = 0
    for path in pop[0]:
        cv2.line(map, citiesInfos[path[0]][2],citiesInfos[path[1]][2],[255,0,0],2)
        cost+= path[2]
        cv2.putText(map, "Iter = " + str(iter), (400, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,
                    [0, 0, 255])
    cv2.putText(map, "Cost = " + str(cost), (400, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,
                [0, 0, 255])

    cv2.imshow("En iyi yol",map)
    cv2.waitKey()

def balancePop(pop, popSize):
    if len(pop)<popSize:
        while len(pop)<popSize:
            pop.append(pop[0])

def main():
    maxLen = 100
    popSize = 10
    mutasyonRatio=40
    imageName = "map.png"

    # Şehir bilgileri ve ulaşım matrisi oluşturulur
    citiesInfos = initCitiesInfos()
    graph = initGraph(citiesInfos)

    map = initImage(imageName, citiesInfos)

    # İlçeler gösterilir
    print("İlçeler gösteriliyor. Seçim yapmak için resme tıklayıp bir tuşa basın")
    cv2.imshow("Ilceler", map)
    cv2.waitKey()

    #Başlangıç ve varış yeri
    currentPlace = int(input("Bulunduğunuz yerin ID'si?"))
    target = int(input("Gitmek istediğiniz yerin ID'si"))

    # Popülasyon oluşturulur
    pop = initPop(popSize, currentPlace, target, maxLen, graph)

    # Popülasyon maliyete göre sıralanır.
    cost, bestCostIndex = createDistanceMatris(pop, graph)
    pop = sortPop(pop, bestCostIndex)
    printPop(pop)

    print("Genetik Algoritması Başlıyor")
    for iter in range(0, 2000):
        pop = crossover(pop)
        mutasyon(popSize, mutasyonRatio, pop, graph, maxLen)

        cost, bestCostIndex = createDistanceMatris(pop, graph)
        pop = sortPop(pop, bestCostIndex)

        if iter%100==0:
            print(iter, "sonunda bulunan en iyi yol çiziliyor.")
            print("Geçmek için resme tıklayıp bir tuşa basınız")
            map = initImage(imageName,citiesInfos)
            drawPaths(map, pop, citiesInfos, iter)


    print("***Genetik algoritması sonlandı***\n")
    print("**Popülasyonun son hali**")
    printPop(pop)
    print("**Maliyet Matrisi**")
    print(cost)
    print("Son durumda en iyi yol")
    map = initImage(imageName,citiesInfos)
    drawPaths(map,pop,citiesInfos, iter)

if __name__ == "__main__":
    main()