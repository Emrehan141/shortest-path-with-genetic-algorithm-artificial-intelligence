# Shortest Path with Genetic Algorithm - Artificial Intelligence

English  | Türkçe
------------- | -------------
This repo contains a program that find shortest path between 2 locations in Konya, Turkey with Genetic Algorithm | Bu repoda Konya'da bulunan 2 ilçe arasındaki en kısa yolu Genetik Algoritması ile bulan bir program bulunmaktadır.

Project partner: İsmail Kalkan